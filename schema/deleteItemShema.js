const deleteItemSchema = {
  params: {
    type: "object",
    properties: {
      id: {
        type: "string"
      }
    }
  },
  response: {
    200: {
      type: "object",
      properties: {
        message: {
          type: "string",
        },
      },
    },
  },
};

module.exports = deleteItemSchema;
