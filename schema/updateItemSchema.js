const updateItemSchema = {
  params: {
    type: "object",
    properties: {
      id: {
        type: "string"
      }
    }
  },
  body: {
    type: "object",
    required: ["name"],
    properties: {
      name: {
        type: "string",
      },
    },
  },
  response: {
    200: {
      type: "object",
      properties: {
        id: {
          type: "string",
        },
        name: {
          type: "string",
        },
      },
    },
  },
};

module.exports = updateItemSchema;
