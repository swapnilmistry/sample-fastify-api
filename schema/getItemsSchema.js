const item = {
  type: "object",
  properties: {
    id: {
      type: "string",
    },
    name: {
      type: "string",
    },
  },
};

const getItemsSchema = {
  schema: {
    response: {
      200: {
        type: "array",
        items: item,
      },
    },
  },
};

module.exports = getItemsSchema;
