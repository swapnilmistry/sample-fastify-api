const fastify = require("fastify")({ logger: true });

const PORT = 8000;

fastify.register(require("fastify-swagger"), {
  exposeRoute: true,
  routePrefix: "/swagger",
  swagger: {
    info: {
      title: "Sample API"
    },
  }
})

fastify.register(require("./routes/index"));

const start = async () => {
  try {
    fastify.listen(PORT);
  } catch (error) {
    fastify.log.error(error);
    process.exit(1);
  }
};

start();
