const getItemsSchema = require("../schema/getItemsSchema");
const getItemsHandler = require("./getItems");

const addItemSchema = require("../schema/addItemSchema");
const addItemHandler = require("./addItem");

const updateItemSchema = require("../schema/updateItemSchema");
const updateItemHandler = require("./updateItem");

const deleteItemSchema = require("../schema/deleteItemShema");
const deleleItemHandler = require("./deleteItem");

function routes(fastify, options, done) {
  // Get all items
  fastify.get("/getItems", {
    schema: getItemsSchema,
    handler: async (request, reply) => {
      const items = await getItemsHandler();
      reply.send(items);
    },
  });

  // Add new item
  fastify.post("/addItem", {
    schema: addItemSchema,
    handler: async (request, reply) => {
      const { name } = request.body;
      const newItem = await addItemHandler(name);
      reply.code(201).send(newItem);
    },
  });

  // Update new item
  fastify.put("/updateItem/:id", {
    schema: updateItemSchema,
    handler: async (request, reply) => {
      const { id } = request.params;
      const { name } = request.body;
      const updatedItem = await updateItemHandler(id, name);
      reply.send(updatedItem);
    },
  });

  // Delete item
  fastify.delete("/delete/:id", {
    schema: deleteItemSchema,
    handler: async (request, reply) => {
      const { id } = request.params;
      await deleleItemHandler(id)
      reply.send({message: `Item ${id} has been removed`});
    },
  });

  done();
}

module.exports = routes;
