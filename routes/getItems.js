const items = require("../db");

const getItemsHandler = () => {
  return items;
};

module.exports = getItemsHandler;
