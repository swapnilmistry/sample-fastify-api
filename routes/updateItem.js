let items = require("../db");

let updateItem = (id, name) => {
  items = items.map((item) => (item.id === id ? { id, name } : item));
  let item = items.find((item) => item.id === id);
  return item;
};

module.exports = updateItem;
