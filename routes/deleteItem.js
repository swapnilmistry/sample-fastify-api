let items = require("../db");

let deleteItem = (id) => {
  items = items.filter((item) => item.id !== id);
  return;
};

module.exports = deleteItem;
