const items = require("../db");
const { v4: uuidv4 } = require("uuid");

const addItemHandler = (name) => {
  const newItem = {
    id: uuidv4(),
    name: name,
  };
  items.push(newItem);
  return newItem;
};

module.exports = addItemHandler;
